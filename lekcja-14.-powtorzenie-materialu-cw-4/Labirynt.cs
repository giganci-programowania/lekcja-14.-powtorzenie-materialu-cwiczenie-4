﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace lekcja_14._powtorzenie_materialu_cw_4
{
    public partial class Labirynt : Form
    {
        public Labirynt()
        {
            InitializeComponent();
            OdNowa();
        }

        private void lblFinish_MouseEnter(object sender, EventArgs e)
        {
            MessageBox.Show("Gratulacje!");
            Close();
        }

        private void lblSciana1_MouseEnter(object sender, EventArgs e)
        {
            OdNowa();
        }

        private void lblSciana2_MouseEnter(object sender, EventArgs e)
        {
            OdNowa();
        }

        private void lblSciana3_MouseEnter(object sender, EventArgs e)
        {
            OdNowa();
        }

        private void lblSciana4_MouseEnter(object sender, EventArgs e)
        {
            OdNowa();
        }

        private void lblSciana5_MouseEnter(object sender, EventArgs e)
        {
            OdNowa();
        }
        
        private void lblSciana6_MouseEnter(object sender, EventArgs e)
        {
            OdNowa();
        }

        private void lblSciana7_MouseEnter(object sender, EventArgs e)
        {
            OdNowa();
        }

        private void OdNowa()
        {
            Point punktPoczatkowy = panelGry.Location;
            punktPoczatkowy.Offset(30, 30);
            Cursor.Position = PointToScreen(punktPoczatkowy);
        }
    }
}
