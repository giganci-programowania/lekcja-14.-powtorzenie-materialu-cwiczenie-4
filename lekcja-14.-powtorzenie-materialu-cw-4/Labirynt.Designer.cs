﻿namespace lekcja_14._powtorzenie_materialu_cw_4
{
    partial class Labirynt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSciana3 = new System.Windows.Forms.Label();
            this.lblSciana7 = new System.Windows.Forms.Label();
            this.lblSciana6 = new System.Windows.Forms.Label();
            this.lblSciana2 = new System.Windows.Forms.Label();
            this.lblSciana5 = new System.Windows.Forms.Label();
            this.lblSciana4 = new System.Windows.Forms.Label();
            this.panelGry = new System.Windows.Forms.Panel();
            this.lblSciana1 = new System.Windows.Forms.Label();
            this.lblFinish = new System.Windows.Forms.Label();
            this.panelGry.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSciana3
            // 
            this.lblSciana3.BackColor = System.Drawing.SystemColors.HotTrack;
            this.lblSciana3.Image = global::lekcja_14._powtorzenie_materialu_cw_4.Properties.Resources.stone_wall_001_pbr_texture_02;
            this.lblSciana3.Location = new System.Drawing.Point(-9, -3);
            this.lblSciana3.Name = "lblSciana3";
            this.lblSciana3.Size = new System.Drawing.Size(26, 404);
            this.lblSciana3.TabIndex = 1;
            this.lblSciana3.MouseEnter += new System.EventHandler(this.lblSciana3_MouseEnter);
            // 
            // lblSciana7
            // 
            this.lblSciana7.BackColor = System.Drawing.SystemColors.HotTrack;
            this.lblSciana7.Image = global::lekcja_14._powtorzenie_materialu_cw_4.Properties.Resources.stone_wall_001_pbr_texture_02;
            this.lblSciana7.Location = new System.Drawing.Point(75, 376);
            this.lblSciana7.Name = "lblSciana7";
            this.lblSciana7.Size = new System.Drawing.Size(734, 25);
            this.lblSciana7.TabIndex = 2;
            this.lblSciana7.MouseEnter += new System.EventHandler(this.lblSciana7_MouseEnter);
            // 
            // lblSciana6
            // 
            this.lblSciana6.BackColor = System.Drawing.SystemColors.HotTrack;
            this.lblSciana6.Image = global::lekcja_14._powtorzenie_materialu_cw_4.Properties.Resources.stone_wall_001_pbr_texture_02;
            this.lblSciana6.Location = new System.Drawing.Point(18, 282);
            this.lblSciana6.Name = "lblSciana6";
            this.lblSciana6.Size = new System.Drawing.Size(734, 25);
            this.lblSciana6.TabIndex = 3;
            this.lblSciana6.MouseEnter += new System.EventHandler(this.lblSciana6_MouseEnter);
            // 
            // lblSciana2
            // 
            this.lblSciana2.BackColor = System.Drawing.SystemColors.HotTrack;
            this.lblSciana2.Image = global::lekcja_14._powtorzenie_materialu_cw_4.Properties.Resources.stone_wall_001_pbr_texture_02;
            this.lblSciana2.Location = new System.Drawing.Point(783, -3);
            this.lblSciana2.Name = "lblSciana2";
            this.lblSciana2.Size = new System.Drawing.Size(26, 404);
            this.lblSciana2.TabIndex = 4;
            this.lblSciana2.MouseEnter += new System.EventHandler(this.lblSciana2_MouseEnter);
            // 
            // lblSciana5
            // 
            this.lblSciana5.BackColor = System.Drawing.SystemColors.HotTrack;
            this.lblSciana5.Image = global::lekcja_14._powtorzenie_materialu_cw_4.Properties.Resources.stone_wall_001_pbr_texture_02;
            this.lblSciana5.Location = new System.Drawing.Point(60, 190);
            this.lblSciana5.Name = "lblSciana5";
            this.lblSciana5.Size = new System.Drawing.Size(734, 25);
            this.lblSciana5.TabIndex = 5;
            this.lblSciana5.MouseEnter += new System.EventHandler(this.lblSciana5_MouseEnter);
            // 
            // lblSciana4
            // 
            this.lblSciana4.BackColor = System.Drawing.SystemColors.HotTrack;
            this.lblSciana4.Image = global::lekcja_14._powtorzenie_materialu_cw_4.Properties.Resources.stone_wall_001_pbr_texture_02;
            this.lblSciana4.Location = new System.Drawing.Point(18, 81);
            this.lblSciana4.Name = "lblSciana4";
            this.lblSciana4.Size = new System.Drawing.Size(734, 25);
            this.lblSciana4.TabIndex = 7;
            this.lblSciana4.MouseEnter += new System.EventHandler(this.lblSciana4_MouseEnter);
            // 
            // panelGry
            // 
            this.panelGry.BackgroundImage = global::lekcja_14._powtorzenie_materialu_cw_4.Properties.Resources.Bermuda_Header;
            this.panelGry.Controls.Add(this.lblSciana5);
            this.panelGry.Controls.Add(this.lblSciana6);
            this.panelGry.Controls.Add(this.lblSciana4);
            this.panelGry.Controls.Add(this.lblSciana1);
            this.panelGry.Location = new System.Drawing.Point(-6, -3);
            this.panelGry.Name = "panelGry";
            this.panelGry.Size = new System.Drawing.Size(815, 462);
            this.panelGry.TabIndex = 8;
            // 
            // lblSciana1
            // 
            this.lblSciana1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.lblSciana1.Image = global::lekcja_14._powtorzenie_materialu_cw_4.Properties.Resources.stone_wall_001_pbr_texture_02;
            this.lblSciana1.Location = new System.Drawing.Point(18, 0);
            this.lblSciana1.Name = "lblSciana1";
            this.lblSciana1.Size = new System.Drawing.Size(776, 22);
            this.lblSciana1.TabIndex = 6;
            this.lblSciana1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSciana1.MouseEnter += new System.EventHandler(this.lblSciana1_MouseEnter);
            // 
            // lblFinish
            // 
            this.lblFinish.Image = global::lekcja_14._powtorzenie_materialu_cw_4.Properties.Resources.c2b2e4ad541a7ce548c5b9a5831af328;
            this.lblFinish.Location = new System.Drawing.Point(-6, 401);
            this.lblFinish.Name = "lblFinish";
            this.lblFinish.Size = new System.Drawing.Size(815, 58);
            this.lblFinish.TabIndex = 0;
            this.lblFinish.MouseEnter += new System.EventHandler(this.lblFinish_MouseEnter);
            // 
            // Labirynt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblSciana2);
            this.Controls.Add(this.lblSciana7);
            this.Controls.Add(this.lblSciana3);
            this.Controls.Add(this.lblFinish);
            this.Controls.Add(this.panelGry);
            this.Name = "Labirynt";
            this.Text = "Labirynt";
            this.panelGry.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblFinish;
        private System.Windows.Forms.Label lblSciana3;
        private System.Windows.Forms.Label lblSciana7;
        private System.Windows.Forms.Label lblSciana6;
        private System.Windows.Forms.Label lblSciana2;
        private System.Windows.Forms.Label lblSciana5;
        private System.Windows.Forms.Label lblSciana1;
        private System.Windows.Forms.Label lblSciana4;
        private System.Windows.Forms.Panel panelGry;
    }
}

